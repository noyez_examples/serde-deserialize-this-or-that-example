extern crate serde;
extern crate serde_json;
#[macro_use] extern crate serde_derive;
use serde::{de, Deserialize, Deserializer };

extern crate interfaces;
use interfaces::Interface;
use std::io::Cursor;

extern crate byteorder;
use byteorder::{ReadBytesExt, BigEndian};



#[derive(Deserialize)]
#[serde(untagged)]
enum U16OrHex { U16(u16), Hex(String) }
pub fn u16_or_hex<'de, D>(deserializer: D) -> Result<u16, D::Error>
    where D: Deserializer<'de>
{
    match U16OrHex::deserialize(deserializer)? {
        U16OrHex::U16(v) => { Ok(v) }
        U16OrHex::Hex(v) => {
            if v.starts_with("0x") {
                u16::from_str_radix(v.trim_left_matches("0x"), 16)
                    .map_err(|e| de::Error::custom(format!("{}",e)))
            }
            else {
                Err(de::Error::custom(format!("no 0x prefix")))
            }
        }
    }
}

#[allow(dead_code)]
fn mac_to_u64<'de, D>(deserializer: D) -> Result<u64, D::Error>
    where D: Deserializer<'de>
{
    let s = String::deserialize(deserializer)?;
    mac_addr_as_u64(&s).ok_or(serde::de::Error::custom("Can't parse MAC address"))
}

pub fn mac_addr_as_u64(iface_str: &String ) -> Option<u64> {
    match Interface::get_by_name(&*iface_str)
    {
        Ok(iface_opt) =>
        {
            if let Some(iface_dev) = iface_opt
            {
                let hw_addr = iface_dev.hardware_addr().unwrap();
                //let bytes = iface_dev.hardware_addr().unwrap().as_bytes();
                let bytes = hw_addr.as_bytes();
                let mut cursor_buf = Cursor::new(bytes);
                let upper_u16 = cursor_buf.read_u16::<BigEndian>().unwrap() as u64;
                let lower_u32 = cursor_buf.read_u32::<BigEndian>().unwrap() as u64;
                Some(upper_u16 << 32 | lower_u32)
            }
            else
            {
                println!("Could not find device: {}", iface_str);
                None
            }
        }
        _ =>
        {
            println!("Could not find interface: {}", iface_str);
            None
        }
    }

}

#[derive(Deserialize)]
#[serde(untagged)]
enum MacOrU64 { U64(u64), Mac(String) }
pub fn mac_or_u64<'de, D>(deserializer: D) -> Result<u64, D::Error>
    where D: Deserializer<'de>
{
    match MacOrU64::deserialize(deserializer)? {
        MacOrU64::U64(v) => { Ok(v) }
        MacOrU64::Mac(v) => {
			mac_addr_as_u64(&v).ok_or(serde::de::Error::custom("Can't parse MAC address"))
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct ThisOrThat {
    #[serde(deserialize_with = "u16_or_hex")]
    value: u16,
    //#[serde(deserialize_with = "mac_to_u64")] // Old deserialize fn
    #[serde(deserialize_with = "mac_or_u64")]
    mac_addr: u64
}

fn main() {

    let test_string_as_u16 = r#"
    { "value": 1234, "mac_addr": "en1" }
    "#;
    println!("Deserializing from str: {}", test_string_as_u16);

    let deserialized: ThisOrThat = serde_json::from_str(test_string_as_u16).unwrap();
    println!("{:#?}", deserialized);



    let test_string_as_hex_string = r#"
    { "value": "0x1234", "mac_addr":"en1" }
    "#;
    println!("Deserializing from str: {}", test_string_as_hex_string);

    let deserialized: ThisOrThat = serde_json::from_str(test_string_as_hex_string).unwrap();
    println!("{:#?}", deserialized);

    let serialized = serde_json::to_string(&deserialized).unwrap();
    println!("serialized: {:#?}", serialized);

    let deserialized: ThisOrThat = serde_json::from_str(&serialized).unwrap();
    println!("againt, deseralized: {:#?}", deserialized);
}
